# it's a hidden layer, we do not whant to push it anywhere
FROM golang:1.14-stretch AS builder

ARG ARG_SECRET

WORKDIR /go/src/gitlab.com/spyzhov/build-args

ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64

COPY ./go.mod ./
RUN go mod download

COPY . .
RUN go build \
        -tags netgo \
        -ldflags "\
            -w \
            -X main.Secret=${ARG_SECRET} \
        " \
        -o /go/bin/example \
        .

# it's a public layer, we will push it to the registry. But `builder` will be lost after all.
FROM debian:9.13-slim
ARG ARG_CORRUPTED
COPY --from=builder /go/bin/example /root/example
ENV ENV_CORRUPTED="${ARG_CORRUPTED}"
CMD ["/root/example"]
