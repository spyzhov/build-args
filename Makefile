CORRUPTED=CORRUPTED_STRING
SECRET=TOP_SECRET

build:
	@docker build \
		--build-arg ARG_CORRUPTED=$(CORRUPTED) \
		--build-arg ARG_SECRET=$(SECRET) \
		-t build-args:example \
		-f Dockerfile .

run: build
	@docker run -it build-args:example

history: build
	@echo "-------------------------------"
	@echo "Search for: $(CORRUPTED)"
	@docker history --no-trunc build-args:example | grep "$(CORRUPTED)" || echo "not found"
	@echo "-------------------------------"
	@echo "Search for: $(SECRET)"
	@docker history --no-trunc build-args:example | grep "$(SECRET)" || echo "not found"
	@echo "-------------------------------"
