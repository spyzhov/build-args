package main

import (
	"log"
	"os"
)

var (
	Secret = "unknown"
)

func main() {
	log.Printf("Corrupted: %s\n", os.Getenv("ENV_CORRUPTED"))
	log.Printf("Secret: %s\n", Secret)
}
